﻿using System;
using System.Runtime.ExceptionServices;

namespace Task9ChaosArray
{
    class Program
    {
        static void Main(string[] args)
        {
            RunProgram();
        }
        //Console based user interface, gives user options to add or get from chaosArray, and prints the array to the console. 
        static void RunProgram()
        {
            ChaosArray<int> cai = new ChaosArray<int>();
            ChaosArray<string> cas = new ChaosArray<string>();
            int res = -1;
            while (res != 3)
            {
                Console.WriteLine("Welcome to ChaosArray, here are your options:");
                Console.WriteLine("1. Add something to the ChaosArray\n2. Get something from the ChaosArray\n3. Exit\nPlease insert your option: ");
                res = Int32.Parse(Console.ReadLine());
                if (res == 1)
                {
                    Console.WriteLine("\nInsert value to be set in ChaosArray: ");
                    string uis = Console.ReadLine();
                    int num;
                    if (Int32.TryParse(uis, out num)) cai.Set(num);
                    else { cas.Set(uis); }
                }
                else if (res == 2)
                {
                    Console.WriteLine("\nChoose 1. to get int, and 2. to get string: ");
                    int uii = Int32.Parse(Console.ReadLine());
                    if (uii == 1) cai.Get();
                    else { cas.Get(); }
                }

                Console.WriteLine("\nLists:");
                int[] arr = cai.getArray();
                for (int i = 0; i < cai.getLength(); i++)
                {
                    Console.Write($"{arr[i]}, ");
                }
                string[] arry = cas.getArray();
                Console.WriteLine();
                for (int i = 0; i < cas.getLength(); i++)
                {
                    Console.Write($"{arry[i]}, ");
                }
                Console.WriteLine();
            }
        }

    }
    class ChaosArray<T>
    {
        T[] chaosArray = new T[4];
        Random r = new Random();

        //Basic set function, checks if the chooses an random index, and checks if its empty. if empty, set, if not catch error. 
        public void Set(T item)
        { 
            try
            {
                int indx = r.Next(0, chaosArray.Length - 1);
                if (chaosArray[indx]==null || chaosArray[indx].Equals(default(T))) chaosArray[indx] = item;
                else { throw new IndexFullException(indx); }
            }catch (IndexFullException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        //Basic get, chooses random index, checks if index has item, if not exception is catched. 
        public T Get()
        {
            T item = default(T);
            try
            {
                int indx = r.Next(0, chaosArray.Length - 1);
                if (chaosArray[indx] == null || chaosArray[indx].Equals(default(T))) throw new IndexEmptyException(indx);
                else { 
                    item = chaosArray[indx];
                    chaosArray[indx] = default(T);
                }
            }
            catch (IndexEmptyException e)
            {
                Console.WriteLine(e.Message);
            }
            return item;
        }

        //help-fuctions
        public T[] getArray()
        {
            return chaosArray;
        }

        public int getLength()
        {
            return chaosArray.Length;
        }
    }

    //Custom errors. 
    class IndexFullException : Exception
    {
        public IndexFullException(int index) : base(String.Format($"Conflict on index {index}, index already full."))
        {

        }
    }

    class IndexEmptyException : Exception
    {
        public IndexEmptyException(int index) : base(String.Format($"Conflict at index {index}, index is empty."))
        {

        }
    }

}
